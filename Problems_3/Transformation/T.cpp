#include <iostream>
#include <fstream>
#include <random>
#include <cmath>



using namespace std;

int main() {

int seed;
double bin[200];
double binstep;
int array[200];
int i, j;
double x[100000];
double y;
double z;


 for (seed=0; seed<100000; seed=seed+1){

   mt19937 rn(seed);	

	 y = rn.max();			
	 z = rn()/y;

	x[seed] =  acos(1-2*z);
			
}

bin[0]=0;
binstep = 3.5/200.0;


for(i=1;i<200;i=i+1){ bin[i] = bin[i-1] + binstep;}

for (i=0; i<200; i=i+1){
			
	for(j=0; j<100000; j=j+1){	
		
			if (bin[i] < x[j] && x[j] < bin[i+1]){ 
	
			array[i] = array[i]+1;}}}

ofstream output("histo1.dat");
    if ( ! output.is_open() ) {
        cout << "Error opening outfile." << endl;
        return 1;}
	
for (i=0; i<200; i=i+1){
	
	output << bin[i] << " " << array[i] << endl;} 

output.close();

    return 0;

}
