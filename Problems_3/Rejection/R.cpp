#include <iostream>
#include <fstream>
#include <random>
#include <cmath>

using namespace std;

int main() {

int seed;
double bin[200];
double binstep;
int array[200];
int i, j;

	for(i=0;i<200; i=i+1){array[i]=0;}


double x;
double y1;
double y2;
double z;
double pi;
double test;

double choice;
double pdf[100000];

pi = 3.14159265359;

bin[0]=0;
binstep = pi/100.0;

for(i=1;i<100;i=i+1){ bin[i] = bin[i-1] + binstep;}



 for (seed=0; seed<100000; seed=seed+1){

   mt19937 rn1(seed);	

	mt19937 rn2(seed+1);

	 y1 = rn1();
	 y2 = rn2();
				
	 z = (y1/rn1.max())*(4.0/pi);

	x =  acos(1.0-(pi/2.0)*z);
   	
	pdf[seed] = (2.0/pi)*sin(x)*sin(x);


	test = (y2/rn2.max())*(2.0/pi)*sin(x);

	//cout << z << "  " << x << "  " << pdf[seed] << "  " << test << "  " << y1/rn1.max()<< "  " << y2/rn2.max() << endl << endl;
	
	if(test < pdf[seed]){ for (i=0; i<100; i=i+1){			
		
			if (bin[i] < x && x < bin[i+1]){ 
	
			array[i] = array[i]+1;
				//cout <<endl<< "this point got binned" << endl;
}}

	}

}

ofstream output("histo2.dat");
    if ( ! output.is_open() ) {
        cout << "Error opening outfile." << endl;
        return 1;}
	
for (i=0; i<100; i=i+1){
	
	output << bin[i] << " " << array[i] << endl;} 

output.close();

    return 0;

}
