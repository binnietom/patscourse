#include <iostream>
#include <fstream>
#include <random>
#include "Testfunction.h"
#include <ctime>

using namespace std;

int main() {

int i, j, f;
double x1[100000], x2[100000], y[100000];
double t, z1, z2, z, p, x1test, x2test, v, v1, v2, x1ave, x2ave;

t = time(0);		//Merssenne-twister random number with clock time
std::mt19937 rn(t);
z1 = rn.max();

x1[0] = 2;
x2[0] = 2;			//start values 
y[0] = rosenbrock(x1[0], x2[0]);

v = 10.0;   //initial variance
v1 = v;
v2 = v;

normal_distribution<double> distribution1(x1[i], v1); //2= s.t. for now..
normal_distribution<double> distribution2(x2[i], v2);

i=1;
f=1;

while(i<100000)
{

	z2 = rn();
	z= z2/z1; 

	x1test = distribution1(rn);		//new random point
	x2test = distribution2(rn);   

	y[i] = rosenbrock(x1test, x2test);   

	p = y[i]/y[i-1];

	if(y[i]>y[i-1]){x1[i] = x1test; x2[i] = x2test; i=i+1;}	//testing the point

	else if(p > z){x1[i] = x1test; x2[i] = x2test; i=i+1;}

	if(i==100 && f==1){    		//remove burn in period
			y[100]=y[0]; x1[100]=x1[0]; x2[100]=x2[0];
	
		for(i=1;i<101;i=i+1)
		
			{y[i]=0; x1[i]=0; x2[i]=0;}i=1; f=0;}  
						
	if(i>100){for(j=(i-100); j<=i; j=j+1){ 
							//adaptive jump size
			x1ave = x1ave +x1[j];
			x2ave = x2ave +x1[j];}
		x1ave = x1ave/100;
		x2ave = x1ave/100;   
			for(j=0; j<=i; j=j+1){
				v1 = (x1[j]-x1ave)*(x1[j]-x1ave);
				v2 = (x2[j]-x2ave)*(x2[j]-x2ave);}
		 v1 = v1/i;
		 v2 = v2/i;} 

cout << i << endl;}  

ofstream output("x1x2.dat");					//open data file    
ofstream output1("x1.dat");
ofstream output2("x2.dat");
ofstream output3("x1x2y.dat");

for(i = 0; i<100000; i=i+1){

output3 << x1[i] << " " << x2[i] << " " << y[i] << endl;
output << x1[i] << " " << x2[i] << endl;
output1 << i << " " << x1[i] << endl;
output2 << i << " " << x2[i] << endl;}

output.close();
output1.close();
output2.close();
output3.close();

 return 0;

}


