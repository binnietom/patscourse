#include <iostream>
#include <cmath>
using namespace std;

extern const int N = 1;   // Number of ODEs
double a = 0;             // Start point
double b = 2;             // End point
double y[N] = {0};        // Initial conditions
int errorflag = 1;        // If '0' use fixed error, if '1' use relative

double coeff = 2/sqrt(M_PI);

double f(double x, double y[], int j) { // x = position, y[](x) = solution vector, j iterates 0:N to select the correct function
  if (j == 0) {
    return coeff*exp(-x*x);
  }
}
