#include <iostream>
#include <fstream>
#include <ctime>
#include <random> 
#include "system.h"
#include "RK45.cpp"
#include "brent.cpp"

using namespace std;

int main() {


double z1,z2,z,t;       //all for preparing the random number [0,1]
t = time(0);		//Merssenne-twister random number with clock time
std::mt19937 rn(t);
z2 = rn.max();

double dy1, dy2, y1, y2, x, y; //as given in the question
double d=0; //dummy variable for swapping window edges if they are the wrong way round..see 'if' below
double e=0.01; //accuracy to give to brent and RK45

double a,b; // solutions of RK45/boundaries for brent initially positive to trigger window selection:

//Creating the window for Brent's Algorithm[a,b]:
z1 = rn();
z = 200.0*z1/z2 -100.0; // set initial values of dy1 and dy2 randomly from guessed y2 [-100,+100] 
y1 = z;
				//RK45 for the interval
a = RK45(y1);
b=a;
while(a*b>0)

{
	z1 = rn();
	z = 200.0*z1/z2 -100.0;    
	y2 = z;

	b = RK45(y2);
cout << a << " " << b << " " << y1 << " " << y2 << endl;
}


if(a>b){d = a; a=b; b=d;}

cout <<"Window ["<<a<<", "<<b << "]" << endl;
cout << 
//input it into brent which finds the soulution of RK45(y2) 

 x, y = brent(y1, y2);



return 0;
}
