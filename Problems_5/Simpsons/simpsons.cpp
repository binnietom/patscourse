#include <iostream>
#include <fstream>

#include "system.h"


using namespace std;

int main() {

int n, a, b;

/* cout << "Please enter the domain [a,b] for which to solve y1 and y2" << endl;
cin >> a >> b ;

cout << "Please the maximum number of data points 'n'" << endl;
cin >> n ;  */

a = 0;
b = 2; 
n = 10000;

double h, x[n], y[n],  dy ;

/*cout << "Thanks, now may I have the bounday conditions y1[0] and y2[0] respectively please." << endl;
cin >> y1[0] >> y2[0];   */

x[0] = a;

y[0] = 0;


h = 0.01 ;   //(b-a)/n;   //start with h to give n=1000

int i;
double y5th, k1,k2,k3, e, del1;

e = 0.01; // epsilon = del0 , setting the relative efficiency for the RK4-5step

for (i=1; i<n; i=i+1){ 
		//Runge-kutta-ing

x[i] = x[i-1] + h;
if(x[i]>b){break;}


		//adjusting the h-step (wrt y) but with the Simpsons collapsed form
			//start with h--> h/2

k1 = h*system1(x[i-1]);
k2 = h*system1(x[i-1]+h/4);

k3 = h*system1(x[i-1]+h/2);

y5th = 	y[i-1] + k1/6 + k2*(2/3) + k3/6;		
 			//2nd half of the step
k1 = h*system1(x[i-1]);
k2 = h*system1(x[i-1]+h/4);

k3 = h*system1(x[i-1]+h/2);

cout << k1 <<"  " << k2 <<"  "<< k3<<"  " << endl<< endl;


y[i] = y[i-1] + k1/6 + k2*(2/3) + k3/6;

y5th = 	y5th + k1/6  + k2*(2/3) + k3/6;



del1 = 	y[i]- y5th;



h = h*pow(((e*y[i])/del1)*(e/del1), 0.1); 


cout << h << "THis is h"<<endl;	
}


			//writing data files
ofstream outputxy1("xy.dat");	
    if ( ! outputxy1.is_open() ) {
        cout << "Error opening outfile." << endl;
        return 1;}	
for (i=0; i<n; i=i+1){	
	outputxy1 << x[i] << " " << 2*y[i] << endl;}   //factor of two for the 								RK-->simpsons conversion
outputxy1.close();		


    return 0;

}


