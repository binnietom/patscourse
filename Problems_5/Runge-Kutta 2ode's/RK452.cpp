#include <iostream>
#include <fstream>

#include "system.h"


using namespace std;

int main() {

int n, a, b;

/* cout << "Please enter the domain [a,b] for which to solve y1 and y2" << endl;
cin >> a >> b ;

cout << "Please the maximum number of data points 'n'" << endl;
cin >> n ;  */

a = 0;
b = 10; 
n = 1000;

double h, x[n], y1[n], y2[n], dy1, dy2;

/*cout << "Thanks, now may I have the bounday conditions y1[0] and y2[0] respectively please." << endl;
cin >> y1[0] >> y2[0];   */

x[0] = a;

y1[0] = 1.5;
y2[0] = 1.5;


h = 0.01 ;   //(b-a)/n;   //start with h to give n=1000

int i;
double y5th, k21, k22, k23, k24, k11, k12, k13, k14, k1,k2,k3,k4, e, del1;

e = 0.1; // epsilon = del0 , setting the relative efficiency for the RK4-5step

for (i=1; i<n; i=i+1){ 
		//Runge-kutta-ing

x[i] = x[i-1] + h;

if(x[i]>b){break;}

k11 = h*system1(x[i-1], y1[i-1], y2[i-1]);
k21 = h*system2(x[i-1], y1[i-1], y2[i-1]);

k12 = h*system1(x[i-1]+h/2, y1[i-1]+k11/2, y2[i-1]+k21/2);
k22 = h*system2(x[i-1]+h/2, y1[i-1]+k11/2, y2[i-1]+k21/2);

k13 = h*system1(x[i-1]+h/2, y1[i-1]+k12/2, y2[i-1]+k22/2);
k23 = h*system2(x[i-1]+h/2, y1[i-1]+k12/2, y2[i-1]+k22/2);

k14 = h*system1(x[i-1]+h, y1[i-1]+k13, y2[i-1]+k23);
k24 = h*system2(x[i-1]+h, y1[i-1]+k13, y2[i-1]+k23);

cout << k21 <<"  " << k22 <<"  "<< k23<<"  " << k24<<"  " << k11<<"  " << k12 <<"  "<< k13<<"  " << k14 <<"  "<< endl<< endl;

y1[i] = y1[i-1] + k11/6 + k12/3 + k13/3 + k14/6;

y2[i] = y2[i-1] + k21/6 + k22/3 + k23/3 + k24/6;


		//adjusting the h-step (wrt y1)
			//start with h--> h/2

k1 = h*system1(x[i-1], y1[i-1], y2[i-1]);
k2 = h*system1(x[i-1]+h/4, y1[i-1]+k11/2, y2[i-1]+k21/2);
k3 = h*system1(x[i-1]+h/4, y1[i-1]+k12/2, y2[i-1]+k22/2);
k4 = h*system1(x[i-1]+h/2, y1[i-1]+k13, y2[i-1]+k23);

y5th = 	y1[i-1] + k1/6 + k2/3 + k3/3 + k4/6;		
 			//2nd half of the step
k1 = h*system1(x[i-1], y5th, y2[i-1]);
k2 = h*system1(x[i-1]+h/4, y5th+k11/2, y2[i-1]+k21/2);
k3 = h*system1(x[i-1]+h/4, y5th+k12/2, y2[i-1]+k22/2);
k4 = h*system1(x[i-1]+h/2, y5th+k13, y2[i-1]+k23);

y5th = 	y5th + k1/6 + k2/3 + k3/3 + k4/6;

del1 = 	y1[i]- y5th;



h = h*pow((e/del1)*(e/del1), 0.2); 


cout << h << "THis is h"<<endl;	
}


			//writing data files
ofstream outputxy1("xy1.dat");	
    if ( ! outputxy1.is_open() ) {
        cout << "Error opening outfile." << endl;
        return 1;}	
for (i=0; i<n; i=i+1){	
	outputxy1 << x[i] << " " << y1[i] << endl;} 
outputxy1.close();		

ofstream outputxy2("xy2.dat");	
    if ( ! outputxy2.is_open() ) {
        cout << "Error opening outfile." << endl;
        return 1;}	
for (i=0; i<n; i=i+1){	
	outputxy2 << x[i] << " " << y2[i] << endl;} 
outputxy2.close();		

ofstream outputy1y2("y1y2.dat");	
    if ( ! outputy1y2.is_open() ) {
        cout << "Error opening outfile." << endl;
        return 1;}	
for (i=0; i<n; i=i+1){	
	outputy1y2 << y1[i] << " " << y2[i] << endl;} 
outputy1y2.close();		
  


    return 0;

}


