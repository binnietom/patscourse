#include <iostream>
#include <cmath>


double system1(double x, double y1, double y2){   // input [x, y1, y2] to get dy1/dx
	double dy1;
	dy1 = -y1*y1 - y2;
	return dy1; }

double system2(double x, double y1, double y2){   // input [x, y1, y2] to get dy2/dx
	double dy2;
	dy2 = 5*y1 - y2;
	return dy2;}


