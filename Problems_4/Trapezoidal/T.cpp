#include <iostream>
#include <fstream>
#include <cmath>
#include "function.h"

using namespace std;


int main() {

double a, b, n, h, A1, B1, x, e, ratio;

double ans = 0.99532226501; //inputting the wolfram alpha answer for reference

cout << "Please enter the limits of integration [a,b] and the required precision 'e' [0,1]:" << endl;
cin >> a >> b >> e;


for (n=1; n<1000000; n=n+1){  //looping over different numbers of n segmentations (max 10^6)
A1=0;
B1=0;


 h = (b-a)/n;


 		
for (x=h; x< b; x=x+h){

//This is the trapezoidal rule

		B1 = B1 + function(x);}
		A1 = h*(B1+ (function(a) + function(b))/2); 

ratio = abs(1 - (A1)/(ans)); //Testing the precision for each iteration of n segmentations


cout << ratio << "  " << A1 << endl;
 
if(ratio<e){break;}

}

cout << "The Area under the graph from Simpson's rule and the segmentation 'n' used are respectively: " << A1 << " and " << n << ". This provides an accuracy ratio of: " << ratio << endl;

  
    return 0;

}


