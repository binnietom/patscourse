#include <iostream>
#include <fstream>
#include <cmath>
#include "function.h"

using namespace std;


int main() {

double a, b, n, h, A2, B2, x, e, ratio;

double ans = 0.99532226501; //inputting the wolfram alpha answer for reference

cout << "Please enter the limits of integration [a,b] and the required precision 'e' [0,1]:" << endl;
cin >> a >> b >> e;



for (n=1; n<1000000; n=n+1){ //looping over different numbers of n segmentations (max 10^6)

B2=0;
A2=0;



 h = (b-a)/n;




double c, d;

d=0;

c = 2*h;
d = (a + h*(n-4));

for (x=h; x< d; x=x+c){

//this is simpson's rule
 		
	B2 = B2 + (4*function(x) +2*function(x+h));	}
	A2 = h*(B2+ (function(a) + 4*function(b-h) + function(b)))/3;

ratio = abs(1-(A2)/(ans)); //Testing the precision for each iteration of n segmentations

cout << ratio << "   " << A2 << endl;

if (ratio<=e){break;}}

cout << "The Area under the graph from Simpson's rule and the segmentation 'n' used are respectively: " << A2 << " and " << n << endl << ". This provides an accuracy ratio of: " << ratio << endl;

  


    return 0;

}


