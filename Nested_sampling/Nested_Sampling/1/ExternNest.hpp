#include <armadillo>
double Nest(double (*likelihood)(const arma::vec& v), arma::vec (*prior)(int), int ns, int steps, const int N, double alpha);
arma::vec matmean(const arma::mat& x);
double mah_d(const arma::mat& x);
double factorial(int x);
double V_e(const arma::mat& x);
double V_sk(const arma::mat& x, const arma::mat& s, int i);
arma::mat elli_sample(const arma::mat& s);



