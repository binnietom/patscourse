#include <iostream>
#include <cmath>
#include <random>
#include <ctime>
#include "MCMC.h"
#include <string>
using namespace std;

double function(double x[]) {
  // Function with solution vector x and of dimension 2.
  // Rosenbrock test function in 2D.
  return 1000 - (1-x[0])*(1-x[0]) - 100*(x[0]*x[0]-x[1])*(x[0]*x[0]-x[1]);
}

int main() {

  const int dim = 2;   // Dimensionality of problem.
  double x[dim], sigma[dim]; // Solution vector & stddev.
  double t = time(0);  // Uses clock time as seed.
  mt19937 gen(t);      // Random number generator. (Mersenne Twister) 
  uniform_real_distribution<double> dis(0, 1); // Specifies range [0,1]
  uniform_real_distribution<double> initiate(-200,200); // Specifies range [-100,100]

  // Initialise random point (x,y) in domain -100:100
  for (int i=0; i<dim; i++) {
    x[i] = initiate(gen);      
    sigma[i] = 3; // Randomly start with stddev up to 3 in both x and y (independently)
  } 

  cout << "Starting points: " << x[0] << " and " << x[1] << endl;
  MCMC(dim, x, sigma, function, 10000, 50, 0.01);

  return 0;
}
