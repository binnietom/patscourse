double MCMC(int dim, double *x, double *sigma, double(*function)(double*), int burn_in, int buffer_len, double threshold);
