#include <iostream>
#include <cmath>
#include <random>

double integrand(double x){   // input x to get y, 
	
double y = (2/(sqrt(M_PI)))*exp(-x*x) ;
	return y; }

	
double pdfinvlinear(double z){ // the linearly decreasing sampling distribution [0,2]
				//(integrated and then inverted)


double x, a, b, c;


	a = -0.48;
	b = 0.98;	
	c = b/a;
	//z=2z;
	
	x = (-c - sqrt(c*c+2*z/a));
	
return x; }

double pdflinear(double z){ // the linearly decreasing sampling distribution [0,2]
				

double x, a, b;


	a = -0.48;
	b = 0.98;	
	
	
	x = a*x+b;
	
return x; }

