#include <iostream>
#include <fstream>
#include <random>
#include "functions2.h"
#include <ctime>

using namespace std;

int main() {

int n, a, b, i;
double e1, e2, x, y, z1, z2, z, t, I, I1, I2, k;

/*cout << "Please enter respectively the domain [a,b] for which to integrate the inputted function." << endl;
cin >> a >> b ;

cout << "Please the starting number of data points 'n' and desired accuracy 'e' respectively." << endl;
cin >> n >> e1;  
*/
			a = 0; b=2; n=10; e1=0.000001;

I1=0;
e2=1;

t = time(0);		//Merssenne-twister random number with clock time
std::mt19937 rn(t);
z2 = rn.max();


ofstream output("xy.dat");					//open data file
    if ( ! output.is_open() ) {
        cout << "Error opening outfile." << endl;
        return 1;}	

while(e1<e2){		

	for(i = 0; i<n; i=i+1){
	
		z1 = rn();
		
		z = z1/z2;			//MC integration using functions.h
		x = pdfinvlinear(z);	  
		k = pdflinear(z);		//normalizing constant k
		y = integrand(x); 
		y = y/(n*k);			//non-linear normalization
		
		
		I1 = I1 + y; 

}
e2 = ((I1-I2)/I1)*((I1-I2)/I1);		// calculated accuracy
		e2 = sqrt(e2);

output << I1 << " "<< I2 << " "<< e2 << " " << i << " " <<endl;   //writing data file
cout << I1 << "   "<< k << "   "<< e2 << "   " << x << "  " <<endl<<endl;
			


I=(I1+I2)/2;		//reset the sum and record the final value
I2 = I1;
I1=0;
n = 2*n;
}

output.close();

cout << "The Integration gives the area under the graph as: " << I << endl;					

    return 0;

}


