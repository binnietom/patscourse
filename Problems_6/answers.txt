1) The code in all 3 directories contains a 1 dimensional MC integration with a user specified sampling distribution and integrand in the 'functions.h' header files. 

2)i) the directory 'flatMCintegration' provides I = 0.995315 with 5368709120 data points, before stalling (not converging).

2)ii) the directory 'linearMCintegration' provides I = 0.995328 0 with 1073741824 data points, before stalling.

by looking at the steps in accuracy (3rd column in xy.dat) I think ~2 more iterations (from ii, ~3 when comparing to i) would provide adequate accuracy (in the flat case) so: 
2^3 x (current number of data points) = ~9x10^10 points.

3) A linear sampling distribution could be used for each splined iteration, i.e. just as the interpolation is a combination of lots of small step linear functions you could sample each of these exactly and iterate for the whole curve.

In 'splineMCintegration' I have had a go at instead binning the random numbers to randomly select a data point from the splined data ('spline_naturalbcs.dat') and use the corresponding y value with a flat sampling distribution. 

This provides I = 0.601945 for 33554432 data points.


(data is saved in the files names 'xy.dat' with columns Inew, Iold, accuracy and iteration number respectively)
