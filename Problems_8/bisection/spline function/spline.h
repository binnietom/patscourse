#include <iostream>


using namespace std;


			//turned my answer to problems_2 into a function h=f(k)
double spline(double k) 
{ 

  double h;
  double x[11]; //xi
  double y[11]; //yi

    x[0]= -2.1;
    x[1]= -1.45;
    x[2]= -1.3;
    x[3]= -0.2;
    x[4]= 0.1;
    x[5]= 0.15;
    x[6]= 0.8;
    x[7]= 1.1;
    x[8]= 1.5; 
    x[9]= 2.8;
    x[10]= 3.8;

    y[0]= 0.012155;
    y[1]= 0.122151;
    y[2]= 0.184520;
    y[3]= 0.960789;
    y[4]= 0.990050;
    y[5]= 0.977751;
    y[6]= 0.527292;
    y[7]= 0.298197;
    y[8]= 0.105399; 
    y[9]= 0.000393669;
    y[10]= 0.0000005355348;

 
  double l[100]; // y  
  double f[100]; // x

  double dy[11]; //d^2y/dy^2  
 dy[0]  =  0.4806;
 dy[1]  =  0.6009;
 dy[2]  =  1.3705;
 dy[3]  =  -1.6155;
 dy[4]  =  -2.1106;
 dy[5]  =  -1.9809;
 dy[6]  =  0.3025;
 dy[7]  =  0.9636;
 dy[8]  =  0.6254;
 dy[9]  =  -0.0804;
 dy[10] =  -0.0390;

 

  int i;
  int j;
  int n;

  double A;
  double B;
  double C;
  double D;



for (n=0; n<11; n=n+1)
	{
	if(k > x[n] &&  k < x[n+1] )
		{


 for (i=0; i<100; i=i+1)
	{ 			//increase i<'100' if you need more precision 							(and don't forget the '100' at the bottom)


f[i] = x[n] + i*(x[n+1]-x[n])/100 ;}
 j=n;

	for (i = 0; i<100; i=i+1)
		{
      
	  A = ((x[j+1]-f[i])/(x[j+1]-x[j])); 
	  B = (1.0 - A);
	  C = ((A*A*A-A)*(x[j+1]-x[j])*(x[j+1]-x[j])/6.0);
	  D = ((B*B*B-B)*(x[j+1]-x[j])*(x[j+1]-x[j])/6.0);
	  

	  l[i] = A*y[j]+B*y[j+1]+C*dy[j]+D*dy[j+1]; 

   	if(x[j] > f[i] || f[i] > x[j+1] ){ j = j+1;}
 
 		} 
	break;  

	}}

for (n=0; n<100; n=n+1){if(k > f[n] &&  k < f[n+1] ){h = l[n]; break;}}

  
  return h;

}


