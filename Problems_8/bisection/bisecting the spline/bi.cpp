#include <iostream>
#include <fstream>
#include <random>
#include "functions.h"

#include <ctime>

using namespace std;

int main() {

double a,b,e, z, z1,z2, y1, y2;

//cout << "Please enter the window for which you definitely know a root exists [a,b]" << endl;
//cin >> a >> b;

//data for the spline is bound by [-2.1,3.8]

a = 0;
b = 2.5;

double a1 = a;
double b1 = b;

char sign;

double t = time(0);		//Merssenne-twister random number with clock time, 
				
std::mt19937 rn(t);
z = rn.max();

z1 = a+(rn()/z)*(b-a);		//zi are the guessed values y = f(z)
z2 = a+(rn()/z)*(b-a);

y1 = spline(z1);
y2 = spline(z2);

//picks 2 opposite signed guesses 
if(y1>0){while(y2>0){z2 = (rn()/z)*(b-a); y2 = spline(z2); sign = '+';}}


if(y1<0){while(y2<0){z2 = (rn()/z)*(b-a); y2 = spline(z2); sign = '-';}}	


double x1,x2,xtest,ytest;				//xi are the bracket [x1,x2] 
x1 = z1;
x2 = z2;

int i;
//while(e< desired accuracy ){					//bisectionning
for(i=0;i<1000000; i++){				//I can't get effeciency working so 								increase i if more d.p. needed --> 								i=log(base2)(accuracy/desiredaccuracy)
xtest = (x1+x2)/2.0;
ytest = spline(xtest);

	if(ytest>0){switch(sign)
		{
		case '+': x1 = xtest; break;
		case '-': x2 = xtest; break;
		}
	}

	if(ytest<0){switch(sign)
		{
		case '+': x2 = xtest; break;
		case '-': x1 = xtest; break;
		}
	}


cout << xtest << " " << ytest << endl;

//e = abs(x2-x1)/(b-a);

}

cout << endl << "the root is at:  " << xtest << endl;

 return 0;

}


