#include <iostream>
#include <fstream>
#include <random>
#include "functions.h"

#include <ctime>

using namespace std;

int main() {

double a, b, c, d, x, e1, e2, z, ya, yb, yc, c1, yx;

char T = 'Y';  //for keeping track of whether bisection or interpolation was done last (Y/N) 

int i;

//cout << "Please enter the window for which you definitely know a root exists [a,b]" << endl;
//cin >> a >> b;

//cout << "Please enter the accuracy to which you would like the root" << endl;
//cin >> e1;

		//data for the spline is bound by [-2.1,3.8]

a=2.1; b=3.7; e1 = 0.00000001;


ya = f(a);
yb = f(b);



if(ya*yb > 0){ cout<<"Root not contained within the Window, please exit and rechoose a and b"<<endl;}
if(abs(ya) < abs(yb)){ z = ya; ya = yb; yb = z; z=0; } 		//swaps a and b if convenient (b>a)
 
c = a;
d = 0;
//Loop from here until abs(b-a) = accuracy 
	//specified user accuracy e1

//while(e2/e1 < 1){

for(i=0; i<1000; i++){

ya = f(a);
yb = f(b);
yc = f(c);

c1 = 3*a/4 +b/4;

	if(ya != yc && yb != yc) // if a,b,c are different fit the polynomial
{x = (a*yb*yc)/((ya-yb)*(ya-yc)) + (ya*b*yc)/((yb-ya)*(yb-yc)) + (ya*yb*c)/((yc-ya)*(yc-yb));} 
	
	else{x = b - yb*(b-a)/(yb-ya);}   // if 2 points are degenerate fit with teh Secant method


	//condition for using the regular linear bisection: if the bounds are decreasing too slowly
if(x<c1 || x>b){x = (a+b)/2; T = 'Y';}			
//.. see answers (f)	
if(T = 'Y' && abs(x-b)>=abs(b-c)/2){x = (a+b)/2; T = 'Y';}	
// checked against previous step size (when bisection was done last)
if(T = 'N' && abs(x-b)>=abs(c-d)/2){x = (a+b)/2; T = 'Y';}	
// checked against previous step size (when bisection was done last)
if(T = 'Y' && abs(b-c)<e1){x = (a+b)/2; T = 'Y';}
// checked against accuracy (when bisection was done last)
if(T = 'N' && abs(c-d)<e1){x = (a+b)/2; T = 'Y';}		
// checked against accuracy (when interpolation was done last)
				
else{ T = 'N';}// no bisection. set flag back to 'No' (interpolation was done last and worked fine)


e2 = abs(f(x)- yx)/abs(f(x));				//accuracy relative to previous loop
yx = f(x);

d = c;
c = b;

if(ya*yx <0){b = x;}else{a = x;}			//checks which side of the root new 
								//point is
ya = f(a);
yb = f(b);

if(abs(ya) < abs(yb)){ z = ya; ya = yb; yb = z; z=0; } 		//swaps a and b if convenient (b>a)

if(e2/e1 < 1){break;}

cout << a << "  "<< b << " " << x << " "<< yx <<" " << e2/e1 << endl;
}//Loop ends here
	

cout << "The Root is: x = " << x << ", with f(x) = "<< yx << endl;

return 0;

}


