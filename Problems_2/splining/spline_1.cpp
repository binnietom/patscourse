#include <iostream>
#include <fstream>
#include <armadillo>


using namespace std;

int main() {

  double x[11]; //xi
  double y[11]; //yi

    x[0]= -2.1;
    x[1]= -1.45;
    x[2]= -1.3;
    x[3]= -0.2;
    x[4]= 0.1;
    x[5]= 0.15;
    x[6]= 0.8;
    x[7]= 1.1;
    x[8]= 1.5; 
    x[9]= 2.8;
    x[10]= 3.8;

    y[0]= 0.012155;

    y[1]= 0.122151;
    y[2]= 0.184520;
    y[3]= 0.960789;
    y[4]= 0.990050;
    y[5]= 0.977751;
    y[6]= 0.527292;
    y[7]= 0.298197;
    y[8]= 0.105399; 
    y[9]= 0.000393669;
    y[10]= 0.0000005355348;

 
  double l[60]; // y  
  double f[60]; // x


  
  double dy[11]; //d^2y/dy^2  (values from y''.dat created from inv.cpp, again
                 //having trouble reading in a data file.

	dy[0]  =       	      0;
  	dy[1]  =	 0.7975;
  	dy[2]  =	 1.3561;
  	dy[3]  =	-1.6096;
  	dy[4]  =	-2.1131;
  	dy[5]  =	-1.9807;
  	dy[6]  =	 0.3022;
   	dy[7]  =	 0.9647;
   	dy[8]  =	 0.6216;
  	dy[9]  =	-0.0708;
    	dy[10] =	      0;
  
 

  int i;
  int j;

  double A;
  double B;
  double C;
  double D;

 for (i=0; i<60; i=i+1){ f[i] = -2.1 + 0.1*i;}


 j=0;

 for (i = 0; i<60; i=i+1){
      
  
  	  

  A = ((x[j+1]-f[i])/(x[j+1]-x[j]));
  B = (1.0 - A);
  C = ((A*A*A-A)*(x[j+1]-x[j])*(x[j+1]-x[j])/6.0);
  D = ((B*B*B-B)*(x[j+1]-x[j])*(x[j+1]-x[j])/6.0);
  
  l[i] = A*y[j]+B*y[j+1]+C*dy[j]+D*dy[j+1]; 

   if(x[j] > f[i] || f[i] > x[j+1] ){ j = j+1;}
 
 }
  


 

ofstream outfile("spline_1.dat");

for(j=0; j<60; j=j+1){
 outfile <<  f[j] << " " << l[j]  << endl;}

outfile.close();
  
  return 0;

}
